package main


import (
    "fmt"
    "net/http"
    )

func monitoraSites() {
    site := "https://faculdadeiv2.com.br/"

    resposta, err := http.Get(site)

    if resposta.StatusCode == 200 {
        fmt.Println("O site foi carregado com sucesso!")
    }else {
        fmt.Println("O site está com problemas. Status Code:", resposta.StatusCode)
    }
    
    if err != nil {
    fmt.Println(err)
    }

    fmt.Println(resposta)
}

func main() {
    monitoraSites()
}