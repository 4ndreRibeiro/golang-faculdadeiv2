package main

import "fmt"

func main() {
    var frutas []string
    frutas = append(frutas, "Morango", "Banana", "Caju", "Jaca", "Jabuticaba", "Pera")
    fmt.Println(len(frutas))
    fmt.Println(cap(frutas))
    fmt.Println(frutas[6])

}