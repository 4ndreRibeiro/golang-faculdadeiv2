package main

import "fmt"

func main() {
    frutas := []string{"Banana", "Pera", "Limão", "Abacaxi"}

    for i, fruta := range frutas {
        fmt.Println("Estou na posição", i, "do meu slice e essa posição tem a fruta", fruta)
       
    }
    //For infinito fica intermitente//
    for {
    fmt.Println("For infinito")
    }
}